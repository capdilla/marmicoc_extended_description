<?php

// Edit name and class according to your files, keep camelcase for class name.

use function Symfony\Component\DependencyInjection\Loader\Configurator\ref;

class Marmicoc_extended_descriptionAjaxModuleFrontController extends ModuleFrontController
{

    const TABLE_NAME = 'product_description_extended';
    const MODULE_IMG_PATH = 'marmicoc_extended_description/views/img/d/';
    const IMG_DIR = _PS_MODULE_DIR_ . self::MODULE_IMG_PATH;

    public function initContent()
    {

        if (Tools::getValue('secure_key') == $this->module->secure_key) {


            header('Content-Type: application/json');
            switch (Tools::getValue('action')) {

                case "getData":
                    die(json_encode($this->getData(Tools::getValue('id_product'))));
                    break;
                case "save":
                    die(json_encode($this->save(Tools::getValue('id_product'))));
                    break;
                default:
                    die(json_encode(["hola" => 'hola']));
                    # code...
                    break;
            }
        }
    }

    private function getData($id_product)
    {
        $result = $this->getOne($id_product);

        if (empty($result)) {
            return null;
        }

        $data = json_decode($result[0]["data"]);

        //parse img path
        $jsonData = array_map(function ($row) {

            if ($row->img != "") {
                $row->img = _PS_BASE_URL_ . __PS_BASE_URI__ . 'modules/' . $row->img;
            }
            return $row;
        }, $data);

        return [
            "active" => $result[0]["active"],
            "data" => $jsonData
        ];
    }

    private function cleanImg($img)
    {

        if ($img == "") {
            return $img;
        }

        $re = '/(.+)\/(modules.+)/m';

        $subst = '\\2';

        return preg_replace($re, $subst, $img);
    }

    private function cleanDescription($description)
    {

        if ($description == "") {
            return $description;
        }

        //replace coma for semicoma
        return str_replace('"', "'", $description);
    }

    public function save($id_product)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {


            $ids = array_keys($_POST["id"]);

            $filesImgKeys = isset($_FILES["file"]) ? array_keys($_FILES["file"]['tmp_name']) : [];

            $data = [];
            $objs = [];

            $skipPos = ['file', 'active'];

            foreach ($_POST as $pos => $values) {
                foreach ($ids as $index => $id) {

                    if (!in_array($pos, $skipPos)) {

                        if ($pos == 'img') {
                            $objs[$id][$pos] = $this->cleanImg($values[$id]);
                        }

                        if ($pos == 'description') {
                            $objs[$id][$pos] = $this->cleanDescription($values[$id]);
                        } else {
                            $objs[$id][$pos] = $values[$id];
                        }
                    }
                }
            }

            foreach ($filesImgKeys as $fileKey) {

                $fileName = $fileKey . ".jpg";

                if (!is_null($_FILES["file"]['tmp_name'][$fileKey])) {
                    $tmp_name = $_FILES["file"]['tmp_name'][$fileKey];
                    move_uploaded_file($tmp_name, self::IMG_DIR . $fileName);

                    $objs[$fileKey]["img"] = self::MODULE_IMG_PATH . $fileName;
                }
            }

            //To array
            foreach (array_keys($objs) as $obj) {
                array_push($data, $objs[$obj]);
            }

            $result = $this->getOne($id_product);

            //create new
            if (empty($result)) {
                $dbData = [
                    'id_product' => (int)$id_product,
                    'active' => (int)$_POST['active'],
                    'data' => json_encode($data, JSON_UNESCAPED_UNICODE)
                ];

                $this->create($dbData);
            } else { //update

                $dbData = [
                    'active' => (int)$_POST['active'],
                    'data' => json_encode($data, JSON_UNESCAPED_UNICODE)
                ];

                $this->update($result[0]["id"], $dbData);
            }
            return $this->getData($id_product);
        }
    }


    private function getOne($id_product)
    {
        $query = new DbQuery();

        $query->select('*');
        $query->from(self::TABLE_NAME);
        $query->where('id_product=' . $id_product);
        $query->limit(1);
        return Db::getInstance()->executeS($query);
    }


    private function create($data)
    {
        return DB::getInstance()->insert(self::TABLE_NAME, $data);
    }

    private function update($id, $data)
    {

        return DB::getInstance()->update(self::TABLE_NAME, $data, 'id=' . $id);
    }
}
