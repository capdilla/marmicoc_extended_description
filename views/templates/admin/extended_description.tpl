<div>
  <div>
    <a
      class="btn btn-primary pointer"
      id="save-extended-description"
      href="#"
      title="Guardar"
    >
      <i class="material-icons">save</i> Guardar
    </a>

    <a
      class="btn btn-primary pointer"
      id="add-new-row"
      href="#"
      title="Agregar Fila"
    >
      <i class="material-icons">add_circle_outline</i> Agregar Fila
    </a>

    <div id="active-continer"></div>
  </div>
  <div id="extended_description" ajax-link="{$ajax_link}"></div>
</div>
