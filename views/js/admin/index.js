var $ = window.$;

(() => {
  function moveArray(arr, old_index, new_index) {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    if (new_index >= arr.length) {
      var k = new_index - arr.length;
      while (k-- + 1) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  }

  function create_UUID() {
    var dt = new Date().getTime();
    var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
      }
    );
    return uuid;
  }

  $("#extended_description").ready(function () {
    const ACTIONS = {
      SET_DATA: "SET_DATA",
      ADD: "ADD",
      REMOVE: "REMOVE",
      CHANGE: "CHANGE",
      TOGGLE_ACTIVE: "TOGGLE_ACTIVE",
      REORDER: "REORDER",
    };

    function reducer(action, state = []) {
      switch (action.type) {
        case ACTIONS.SET_DATA:
          return {
            ...state,
            ...action.payload,
          };
        case ACTIONS.ADD:
          return {
            ...state,
            data: [
              ...state.data,
              { img: "", description: "", id: create_UUID() },
            ],
          };
        case ACTIONS.REMOVE:
          return {
            ...state,
            data: state.data.filter((d, key) => key !== action.payload.index),
          };
        case ACTIONS.CHANGE:
          const newData = [...state.data];

          newData[action.payload.index][action.payload.key] =
            action.payload.value;

          return {
            ...state,
            data: newData,
          };
        case ACTIONS.TOGGLE_ACTIVE:
          return {
            ...state,
            active: action.payload,
          };
        case ACTIONS.REORDER:
          return {
            ...state,
            data: moveArray(
              state.data,
              action.payload.oldIndex,
              action.payload.newIndex
            ),
          };
        default:
          return state;
      }
    }

    const createStore = (reducer, initialState) => {
      return {
        _subscriber: (newState) => {},
        state: initialState,
        dispatch(action) {
          const newState = reducer(action, this.state);
          this.state = newState;
          this._subscriber(newState);
        },
        subscribe(fn) {
          this._subscriber = fn;
          fn(this.state);
        },
        getState() {
          return this.state;
        },
      };
    };

    const fetchData = (url, type = "GET", data) => {
      return new Promise((resolve, reject) => {
        $.ajax({
          url,
          type,
          headers: { "cache-control": "no-cache" },
          data,
          processData: false,
          contentType: false,
          success: function (response) {
            resolve(response);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            reject();
          },
        });
      });
    };

    const initialState = {
      data: [{ img: "", description: "", id: create_UUID() }],
      active: 0,
    };

    const url = $("#extended_description").attr("ajax-link");

    const store = createStore(reducer, initialState);

    //init function
    function getData() {
      const urls = [`${url}&action=getData`];

      const result = Promise.all(urls.map((u) => fetchData(u)));

      result.then(([data]) => {
        store.dispatch({
          type: ACTIONS.SET_DATA,
          payload: {
            data: data.data,
            active: data.active,
          },
        });
      });
    }
    getData();

    const ImageInput = (index, value, onChange) => {
      const div = document.createElement("div");
      div.className = "col-sm";

      const img = document.createElement("img");

      img.className = "img-description";

      const deleteImg = document.createElement("a");
      deleteImg.className = "btn pointer";
      deleteImg.innerHTML = '<i class="material-icons">delete</i>';
      deleteImg.onclick = function () {
        onChange(index, "file", null);
        onChange(index, "img", "");
      };

      const joinimgBtn = (img, btn) => {
        const div = document.createElement("div");

        div.appendChild(img);

        div.appendChild(btn);

        return div;
      };

      if (value.file) {
        const reader = new FileReader();

        reader.readAsDataURL(value.file);

        reader.onload = function (e) {
          img.src = e.target.result;
        };

        div.appendChild(joinimgBtn(img, deleteImg));

        return div;
      }

      if (value.img) {
        img.src = value.img;
        div.appendChild(joinimgBtn(img, deleteImg));

        return div;
      }

      const divFile = document.createElement("div");
      divFile.className = "custom-file";

      const imageInput = document.createElement("input");
      imageInput.className = "custom-file-input";

      imageInput.type = "file";

      imageInput.onchange = function (e) {
        onChange(index, "file", e.target.files[0]);
      };

      const label = document.createElement("label");

      label.className = "custom-file-label";

      label.innerText = "Seleccionar archivo(s)";

      divFile.appendChild(imageInput);
      divFile.appendChild(label);

      div.appendChild(divFile);

      return div;
    };

    const TextArea = (index, value, onChange) => {
      const textArea = document.createElement("textarea");

      textArea.className = "form-control description-row-textarea";

      textArea.value = value.description;

      textArea.onchange = function (e) {
        onChange(index, "description", e.target.value);
      };

      return textArea;
    };

    const Row = (key, data, onRowChange) => {
      const div = document.createElement("div");
      div.className = "description-row";

      const imageInput = ImageInput(key, data, onRowChange);
      const textArea = TextArea(key, data, onRowChange);

      const a = document.createElement("a");
      a.className = "btn pointer";
      a.innerHTML = '<i class="material-icons">delete</i>';
      a.onclick = function () {
        store.dispatch({ type: ACTIONS.REMOVE, payload: { index: key } });
      };

      const divUpDown = document.createElement("div");
      divUpDown.style = "display: grid;";

      const up = document.createElement("a");
      up.className = "btn pointer";
      up.innerHTML = '<i class="material-icons">arrow_circle_up</i>';
      up.onclick = function () {
        store.dispatch({
          type: ACTIONS.REORDER,
          payload: { oldIndex: key, newIndex: key - 1 },
        });
      };

      const down = document.createElement("a");
      down.className = "btn pointer";
      down.innerHTML = '<i class="material-icons">arrow_circle_down</i>';
      down.onclick = function () {
        store.dispatch({
          type: ACTIONS.REORDER,
          payload: { oldIndex: key, newIndex: key + 1 },
        });
      };

      if (key > 0) {
        divUpDown.appendChild(up);
      }

      if (key < store.getState().data.length - 1) {
        divUpDown.appendChild(down);
      }

      if (key % 2 == 0) {
        div.appendChild(imageInput);
        div.appendChild(textArea);
      } else {
        div.appendChild(textArea);
        div.appendChild(imageInput);
      }

      div.appendChild(a);
      div.appendChild(divUpDown);

      return div;
    };

    function save() {
      const data = store.getState().data;

      const formData = new FormData();

      formData.append("active", store.getState().active);

      data.forEach((d, key) => {
        Object.keys(d).forEach((k) => {
          formData.append(`${k}[${d.id}]`, d[k]);
        });
      });

      fetchData(`${url}&action=save`, "POST", formData)
        .then((response) => {
          $.growl.notice({
            title: "Guardado con exito",
            size: "large",
            message: "Cambios guardados",
          });
        })
        .catch(() => {
          $.growl.error({
            title: "Ha ocurrido un erro",
            size: "large",
            message: "los cambios no se han podido guardar.",
          });
        });
    }

    const onRowChange = (index, key, value) => {
      store.dispatch({ type: ACTIONS.CHANGE, payload: { index, key, value } });
    };

    const activeButton = (value) => {
      const maindiv = document.createElement("div");
      // maindiv.className = "form-group row";

      const divCol = document.createElement("div");
      divCol.className = "col-sm";

      const divInput = document.createElement("div");
      divInput.className = "input-group";

      const spanSwitch = document.createElement("span");
      spanSwitch.className = "ps-switch";
      spanSwitch.onclick = function () {
        store.dispatch({ type: ACTIONS.TOGGLE_ACTIVE, payload: value ? 0 : 1 });
      };

      const createInput = () => {
        return `
          <input value="0" ${value == "0" && "checked"} type="radio" />
          <label>No</label>
    
          <input value="1" ${value == "1" && "checked"}  type="radio" />
          <label>Sí</label>
          <span class="slide-button"></span>
        `;
      };

      spanSwitch.innerHTML = createInput();

      divInput.appendChild(spanSwitch);

      divCol.appendChild(divInput);

      maindiv.innerHTML = ` <label for="" class="form-control-label">Activado</label>`;

      maindiv.appendChild(divCol);

      return maindiv;
    };

    $("#save-extended-description").click(function () {
      save();
    });

    $("#add-new-row").click(function () {
      store.dispatch({ type: ACTIONS.ADD });
    });

    store.subscribe(function (state) {
      $("#active-continer").empty();
      $("#active-continer").append(activeButton(state.active));
      $("#extended_description").empty();
      const rows = state.data.map((d, key) => Row(key, d, onRowChange));
      $("#extended_description").append(rows);
    });
  });
})();
