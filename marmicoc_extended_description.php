<?php

if (!defined('_PS_VERSION_'))
    exit();

class Marmicoc_Extended_Description extends Module
{
    public function __construct()
    {
        $this->name = 'marmicoc_extended_description';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Diatomea Tech';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();


        $this->secure_key = Tools::encrypt($this->name);


        $this->displayName = $this->l('Marmicoc descripcion extendida');
        $this->description = $this->l('Módulo para la gestión de descripcion extendida');
    }


    public function install()
    {

        return parent::install() &&
            $this->addTable() &&
            $this->registerHook(['displayAdminProductsExtra', 'actionAdminControllerSetMedia']);
    }

    public function uninstall()
    {
        return parent::uninstall();
    }


    public function hookActionAdminControllerSetMedia($params)
    {


        if ($this->context->controller->controller_name == 'AdminProducts') {
            $this->context->controller->addCSS(_PS_CSS_DIR_ . 'jquery.fancybox.css', 'screen');
            $this->context->controller->addJqueryPlugin('fancybox');

            $this->context->controller->addJS($this->_path . 'views/js/admin/index.js');
            $this->context->controller->addCSS($this->_path . 'views/css/admin/index.css');
        }
    }

    public function hookDisplayAdminProductsExtra($params)
    {

        $id_product = Tools::getValue('id_product', isset($params['id_product']) ? $params['id_product'] : null);

        $ajax_link = $this->context->link->getModuleLink(
            'marmicoc_extended_description',
            'ajax',
            [
                'secure_key' => $this->secure_key,
                'id_product' => $id_product
            ]
        );

        $this->context->smarty->assign(
            array(
                'ajax_link' => $ajax_link
            )
        );


        return $this->display(__FILE__, './views/templates/admin/extended_description.tpl');
    }

    private function addTable()
    {
        return DB::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'product_description_extended` (
            `id` INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
            `id_product` INT(11) unsigned NOT NULL,
            `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
            `data` JSON NOT NULL,

            CONSTRAINT `' . _DB_PREFIX_ . 'product_description_extended_id_product`
            FOREIGN KEY (`id_product`) REFERENCES `' . _DB_PREFIX_ . 'product`(`id_product`) 
            ON DELETE CASCADE ON UPDATE CASCADE

        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    }
}
