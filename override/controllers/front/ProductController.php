<?php

class ProductController extends ProductControllerCore
{

    function transformDescriptionWithImg($desc)
    {
        $extendedInfo = $this->getExtendedDescription();
        if ($extendedInfo != false) {
            return $this->createExtendedDescription($extendedInfo);
        }

        $reg = '/\[img\-([0-9]+)\-(left|right)\-([a-zA-Z0-9-_]+)\]/';
        while (preg_match($reg, $desc, $matches)) {
            $link_lmg = $this->context->link->getImageLink($this->product->link_rewrite, $this->product->id . '-' . $matches[1], $matches[3]);
            $class = $matches[2] == 'left' ? 'class="imageFloatLeft"' : 'class="imageFloatRight"';
            $html_img = '<img src="' . $link_lmg . '" alt="" ' . $class . '/>';
            $desc = str_replace($matches[0], $html_img, $desc);
        }

        return $desc;
    }


    private function getExtendedDescription()
    {

        $query = new DbQuery();

        $query->select('*');
        $query->from('product_description_extended');
        $query->where('id_product=' . $this->product->id);
        $query->limit(1);
        $result  = Db::getInstance()->executeS($query);

        if (empty($result)) {
            return false;
        }

        if ($result[0]["active"] == 0) {
            return false;
        }

        $data = json_decode($result[0]["data"]);

        //parse img path
        $jsonData = array_map(function ($row) {
            if ($row->img != "") {
                $row->img = _PS_BASE_URL_ . __PS_BASE_URI__ . 'modules/' . $row->img;
            }
            return $row;
        }, $data);


        return $jsonData;
    }


    private function createExtendedDescription($data)
    {
        $html = '';
        foreach ($data as $index => $d) {

            if ($d->img == "") {
                $html = $html . $this->createOnlyDescription($d->description);
            }

            if ($index % 2 == 0 && $d->img != "") {
                $html = $html . $this->createLeft($d->img, $d->description);
            }

            if ($index % 2 == 1 && $d->img != "") {
                $html = $html . $this->createRight($d->img, $d->description);
            }
        }
        return $html;
    }

    private function createOnlyDescription($description)
    {
        return '
            <div class="uk-card-body" style="word-wrap: break-word;width: 100%;margin: 0 auto;text-align: justify;">
            <p>' . $description . '</p>
            </div>
        ';
    }
    private function createLeft($img, $description)
    {
        return '
        <div class="uk-card uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid style="min-height: 10em;">
            <div class="uk-card-media-left uk-cover-container uk-flex uk-flex-center uk-flex-middle">
                <img
                    src="' . $img . '"
                    class="uk-cover"
                    style="width: 300px!important;height: 150px;object-fit: contain;"
                />
            </div>
            <div>
                <div class="uk-card-body">
                    <p>' . $description . '</p>
                </div>
            </div>
        </div>';
    }

    private function createRight($img, $description)
    {
        return '
        <div class="uk-card  uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid style="min-height: 10em;">
            <div class="uk-flex-last@s uk-card-media-right uk-cover-container uk-flex uk-flex-center uk-flex-middle">
                <img
                    src="' . $img . '"  
                    style="width: 300px!important;height: 150px;object-fit: contain;"
                />
                
            </div>
            <div>
                <div class="uk-card-body">
                    <p style="text-align: justify;">' . $description . '</p>
                </div>
            </div>
        </div>
    ';
    }
}
